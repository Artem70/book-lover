//
//  CoreDataStack.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    // Singleton
    static let instance = CoreDataStack()
    
    private init() {}
    
    
    func context () -> NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    
//MARK: Book
    func fetchData () -> [Book] {
        
        var book: [Book] = []
        
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        
        do {
            let items = try context().fetch(fetchRequest)
            book = items
            
        } catch {
            print(error.localizedDescription)
        }
        return book
    }
    
    
    func fetchDataSearch (_ searchString: String?) -> [Book] {
        
        var book: [Book] = []
        
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        
        if let validString = searchString {
            fetchRequest.predicate = NSPredicate(format: "name CONTAINS[cd] %@", validString)
        }
        do {
            let items = try context().fetch(fetchRequest)
            book = items
            
        } catch {
            print(error.localizedDescription)
        }
        return book
    }
    
    
    func fetchSegmentControl (valueBook: Bool) -> [Book] {
        
        var book: [Book] = []
        
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        
        do {
            let items = try context().fetch(fetchRequest)
            
            for value in items {
                
                if value.readBook == valueBook {
                    book.append(value)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
        return book
    }
    
    
    func saveCreateNewBook(name:String, author:String, whoAdvised:String) {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Book", in: context()) else {
            fatalError("can not create entity description")
        }
        
        let object = NSManagedObject(entity: entity, insertInto: context()) as! Book
        
        object.name = name
        object.author = author
        object.whoAdvised = whoAdvised
        
        do {
            try object.managedObjectContext?.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func saveDataJSON (name:String, author:String, whoAdvised:String, rating: Float, readBook: Bool) {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Book", in: context()) else {
            fatalError("can not create entity description")
        }
        
        let object = NSManagedObject(entity: entity, insertInto: context()) as! Book
        
        object.name = name
        object.author = author
        object.whoAdvised = whoAdvised
        object.rating = rating
        object.readBook = readBook
        
        do {
            try object.managedObjectContext?.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func createEntitty () -> Book {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Book", in: context()) else {
            fatalError("can not create entity description")
        }
        let object = NSManagedObject(entity: entity, insertInto: context()) as! Book
    
        return object
    }
    
    func updateBook (name:String, author:String, whoAdvised:String, rating: Float, readed: Bool) {
        
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        
        do {
            let items = try context().fetch(fetchRequest)
            
            if items.count > 0 {
                let book = items.first!
                book.name = name
                book.author = author
                book.whoAdvised = whoAdvised
                book.rating = rating
                book.readBook = readed
                
                do {
                    try context().save()
                } catch {
                    print(error.localizedDescription)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "BookLover")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
