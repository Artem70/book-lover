//
//  BookCell.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

import UIKit

protocol BookCellDelegate: class {
    
    func bookCell(cell: BookCell, buttonPressed: UIButton)
}

class BookCell: UITableViewCell {
    
    var showContactsButton: Bool {
        set {
            textField.rightViewMode = newValue ? .always : .never
        }
        get {
            return textField.rightViewMode == .always
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    fileprivate var contactsButton: UIButton = UIButton(type: .custom)
    
    weak var delegateBookCell: BookCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contactsButton.setImage(#imageLiteral(resourceName: "address-book"), for: .normal)
        contactsButton.addTarget(self,
                                 action: #selector(contactsButtonPressed(_:)),
                                 for: .touchUpInside)
        contactsButton.imageView?.contentMode = .scaleAspectFit
        contactsButton.frame = CGRect(x: 0.0, y: 0.0,
                                      width: textField.frame.height,
                                      height: textField.frame.height)
        textField.rightView = contactsButton
    }
    
    override var isFirstResponder: Bool {
        return textField.isFirstResponder
    }
    
    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK: Actions
private extension BookCell {
    
    @objc func contactsButtonPressed(_ sender: UIButton) {
        delegateBookCell?.bookCell(cell: self, buttonPressed: sender)
    }
}

