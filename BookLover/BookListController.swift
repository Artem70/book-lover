//
//  BookListController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 13.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import FloatRatingView
import CoreData

class BookListController: BaseController {
    
    @IBOutlet weak var loadlabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet fileprivate weak var segmentOutlet: UISegmentedControl!
    
    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        reloadData()
    }
    
    var filteredArray: [Book] = []
    
    enum State {
        case normal
        case search
        
        func searchIsActive() -> Bool {
            switch self {
            case .normal:
                return  false
            case .search:
                return true
            }
        }
    }
    
    var searchState: State = .normal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        reloadData()
        setupView()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.tabBarItem = UITabBarItem(title: "Книги", image: #imageLiteral(resourceName: "books") , tag: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: Load Data
extension BookListController {
    
    func reloadData() {
        
        switch searchState {
        case .normal:
            if segmentOutlet.selectedSegmentIndex == 0 {
                filteredArray = CoreDataStack.instance.fetchSegmentControl(valueBook: false)
            } else if segmentOutlet.selectedSegmentIndex == 1 {
                filteredArray = CoreDataStack.instance.fetchSegmentControl(valueBook: true)
            }
        case .search:
            if searchBar.text != "" {
                filteredArray = CoreDataStack.instance.fetchDataSearch(searchBar.text!)
            } else {
                filteredArray = CoreDataStack.instance.fetchData()
            }
        }
        tableView.reloadData()
    }
    
    func loadData() {
        
        self.activityIndicator.startAnimating()
        self.setupIsHidden(bool: true)
        //TODO: show error
        filteredArray = CoreDataStack.instance.fetchData()
        setupLoadMenu()
    }
}


//MARK: Setup
extension BookListController {
    
    func setupView() {
        
        setupBarButtonItems()
        setupSearchBar()
        setupTableView()
        setupSegmentOutlet()
    }
    
    func setupSegmentOutlet() {
        
        segmentOutlet.tintColor = AppColor.greenColor()
        segmentOutlet.setTitle("Хочу прочитать", forSegmentAt: 0)
        segmentOutlet.setTitle("Прочитанные", forSegmentAt: 1)
    }
    
    func setupSearchBar() {
        
        searchBar.isHidden = true
        searchBar.delegate = self
        searchBar.barTintColor = AppColor.greenColor()
        searchBar.tintColor = UIColor.white
        definesPresentationContext = true
    }
    
    func setupIsHidden (bool: Bool) {
        
        self.tableView.isHidden = bool
        self.navigationController?.isNavigationBarHidden = bool
        self.segmentOutlet.isHidden = bool
        self.tabBarController?.tabBar.isHidden = bool
    }
    
    func setupLoadMenu()  {
        
        self.activityIndicator.stopAnimating()
        self.setupIsHidden(bool: false)
        self.activityIndicator.isHidden = true
        self.loadlabel.isHidden = true
        self.reloadData()
        self.tableView.reloadData()
    }
    
    func setupTableView() {
        
        let nib = UINib(nibName: "BookListCell", bundle: Bundle.main)
        tableView.register(nib, forCellReuseIdentifier: "BookListCell")
        
        let nibSearch = UINib(nibName: "BookSearchCell", bundle: Bundle.main)
        tableView.register(nibSearch, forCellReuseIdentifier: "BookSearchCell")
        tableView.tableFooterView = UIView()
    }
    
    func setupBarButtonItems() {
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .search,
            target: self,
            action: #selector(searchButtonPressed(_:)))
        
        //        let buttonSearch = UIButton(type: .custom)
        //        buttonSearch.setImage(#imageLiteral(resourceName: "search"), for: .normal)
        //        buttonSearch.sizeToFit()
        //        buttonSearch.addTarget(self, action: #selector(searchButtonPressed(_:)), for: .touchUpInside)
        //        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buttonSearch)
        //
        
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        button.sizeToFit()
        button.addTarget(self, action: #selector(addButtonPressed(_:)), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.title = "Список книг"
        
    }
}

//MARK: Action
extension BookListController {
    
    @objc func addButtonPressed(_ sender: UIButton) {
        
        let vc = AddBookController()
        // delegate protocol BookListDelegate
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func searchButtonPressed(_ sender: UIBarButtonItem) {
        
        searchState = .search
        searchBar.isHidden = false
        reloadData()
        navigationItem.leftBarButtonItem = nil
        navigationItem.rightBarButtonItem = nil
        self.navigationItem.title = "Поиск"
        segmentOutlet.isHidden = true
        searchBar.delegate = self
        searchBar.showsCancelButton = true
    }
}


//MARK: UITableViewDataSource, UITableViewDelegate
extension BookListController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let book = filteredArray[indexPath.row]
        
        switch searchState {
        case .normal:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BookListCell.reuseIdentifier(), for: indexPath) as! BookListCell
            
            cell.book = book
            
            return cell
            
        case .search:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BookSearchCell.reuseIdentifier(), for: indexPath) as! BookSearchCell
            
            cell.bookSearch = book
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true )
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        let vc = BookDetailController()
        vc.delegate = self
        
        vc.book = filteredArray[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: editActionsForRowAt
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let share = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Поделиться") { (action, indexPath) in
            
            var text: String = ""
            
            if self.filteredArray[indexPath.row].readBook{
                
                text = "Я прочитал книгу " + self.filteredArray[indexPath.row].name!
            } else {
                
                text = "Я хочу прочитать книгу " + self.filteredArray[indexPath.row].name!
            }
            
            let defaultText = text
            
            let activityController = UIActivityViewController (activityItems: [defaultText], applicationActivities: nil)
            
            self.present(activityController, animated: true, completion: nil)
        }
        
        let delete = UITableViewRowAction(style: .default, title: "Удалить") {( action, indexPath) in
            
            let alert = UIAlertController(title: "Вы точно хотите удалить", message: nil, preferredStyle: .actionSheet)
            
            let deleteAction = UIAlertAction(title: "Удалить", style: .destructive, handler: { action in
                
                let context = CoreDataStack.instance.context()
                context.delete(self.filteredArray[indexPath.row])
                CoreDataStack.instance.saveContext()
                self.reloadData()
            })
            
            let cancelAction = UIAlertAction(title: "Отмена", style: .default, handler: nil)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        share.backgroundColor = UIColor.blue
        delete.backgroundColor = UIColor.red
        return [delete, share]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredArray.count
    }
}


//MARK: BookListDelegate
extension BookListController: BookListDelegate {
    
    func bookArr(barButtonPressed: UIBarButtonItem) {
        reloadData()
    }
}
//MARK: RatingDelegate
extension BookListController: BookDetailControllerDelegate {
    
    func detailControllerDidChangeBook(_ book: Book) {
        reloadData()
    }
}

//MARK: UISearchResultsUpdating
extension BookListController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        reloadData()
    }
}

extension BookListController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = nil
        searchState = .normal
        view.endEditing(true)
        setupBarButtonItems()
        searchBar.isHidden = true
        segmentOutlet.isHidden = false
        reloadData()
        
    }
}

