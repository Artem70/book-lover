//
//  AddBookController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 10.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import Contacts

protocol BookListDelegate {
    
    func bookArr(barButtonPressed: UIBarButtonItem)
}

class AddBookController: BaseController, ActiveFieldFrameGetter{
    
    var delegate: BookListDelegate?
    
    var objects: [Data] = [
        Data(type: .Name, value: nil),
        Data(type: .Author, value: nil),
        Data(type: .WhoAdvised, value: nil),
        ]
    
    struct Data {
        let type: DataType
        var value: String?
    }
    
    enum DataType: Int {
        case Name
        case Author
        case WhoAdvised
        
        func stringValue() -> String {
            switch self {
            case .Name:
                return "Название"
            case .Author:
                return "Автор"
            case .WhoAdvised:
                return "Кто посоветовал"
            }
        }
        
        var showConactsButton: Bool {
            switch self {
            case .Author,
                 .Name:
                return false
            case .WhoAdvised:
                return true
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var keyboardNotificationHandler: KeyboardNotificationHandler = KeyboardNotificationHandler()
    
    var activeFieldViewFrame: CGRect? {
        return tableView.visibleCells.first(where: { $0.isFirstResponder })?.frame
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
        
        keyboardNotificationHandler.subscribeToKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        
        keyboardNotificationHandler.unSubscribeFromKeyboardNotifications()
    }
}

//MARK: Setup
extension AddBookController {
    
    func setupView() {
        setupNavigationItem()
        setupTableView()
    }
    
    func setupNavigationItem() {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(saveButtonPressed(_:)))
        
        
        self.navigationItem.title = "Добавить книгу"
    }
    
    func setupTableView() {
        
        tableView.register(BookCell.nib(), forCellReuseIdentifier: BookCell.reuseIdentifier())
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.keyboardDismissMode = .onDrag
        
        keyboardNotificationHandler.setup(withView: view,
                                          scrollView: tableView,
                                          activeFrameGetter: self)
        
    }
    
}


//MARK: UITextFieldDelegate
extension AddBookController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        objects[textField.tag].value = textField.text
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}



//MARK: UITableViewDataSource, UITableViewDelegate
extension AddBookController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = objects[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.reuseIdentifier(), for: indexPath) as! BookCell
        
        cell.nameLabel.text = obj.type.stringValue()
        cell.textField.text = obj.value
        cell.textField.tag = obj.type.rawValue
        cell.textField.autocapitalizationType = .sentences
        cell.textField.delegate = self
        cell.delegateBookCell = self
        cell.showContactsButton = obj.type.showConactsButton
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
        
    }
}
//MARK: BookListDelegate
extension AddBookController {
    
    @objc func saveButtonPressed(_ sender: UIBarButtonItem) {
        
        self.view.endEditing(true)
        
        var isValid = true
        for obj in objects {
            if obj.value == nil || obj.value == "" {
                isValid = false
                break
            }
        }
        
        if isValid {
            
            var nameObj = ""
            var authorObj = ""
            var whoAdvisedObj = ""
            
            for bookObj in self.objects {
                switch bookObj.type {
                case .Name:
                    nameObj = bookObj.value!
                case .Author:
                    authorObj = bookObj.value!
                case .WhoAdvised:
                    whoAdvisedObj = bookObj.value!
                }
            }
            
            CoreDataStack.instance.saveCreateNewBook(name: nameObj, author: authorObj, whoAdvised: whoAdvisedObj)
            
            delegate?.bookArr(barButtonPressed: sender)
            self.navigationController?.popViewController(animated: true)
            
        } else {
            showAlertError(mesageTitle: "Заполните все поля", message: "Попробуйте еще раз")
        }
    }
}

//MARK: ContactsControllerDelegate
extension AddBookController: ContactsControllerDelegate {
    
    func contactsValue(value: String) {
        
        self.view.endEditing(true)
        
        objects[2].value = value
        tableView.reloadData()
    }
}

//MARK: WhoAdvisedCellDelegate
extension AddBookController: BookCellDelegate {
    
    func bookCell(cell: BookCell, buttonPressed: UIButton) {
        
        self.view.endEditing(true)
        
        let vc = ContactsController()
        
        vc.contactsDelegate = self
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


