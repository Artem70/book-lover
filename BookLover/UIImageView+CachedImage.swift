//
//  UIImageView+CachedImage.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//


import UIKit

extension UIImageView {
    
    func setImage(url: String?) {
        
        guard let validUrl = url else {
            return
        }
        let pathToImage = FileManager.pathToImage(avatar_url: validUrl)
        
        if FileManager.default.fileExists(atPath: pathToImage) {
            image = UIImage(contentsOfFile: pathToImage)
        } else {
            imageFromUrl(urlString: url, completionHandler: { (image) in
                self.image = image
                
                FileManager.saveImageDocumentDirectory(image: image!, avatar_url: url!)
            })
        }
    }
}
