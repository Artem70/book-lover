//
//  UserRegisterController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 11.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import CoreData

class UserRegisterController: BaseController, ActiveFieldFrameGetter {
    
    @IBOutlet weak var entry: UIButton!
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var miss: UIButton!
    @IBOutlet weak var emailTextField: TextField!
    @IBOutlet weak var passwordTextField: TextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    fileprivate var keyboardNotificationHandler: KeyboardNotificationHandler = KeyboardNotificationHandler()
    
    var activeFieldViewFrame: CGRect? {
        guard let activeField = activeField() else {
            return nil
        }
        return activeField.convert(activeField.frame, to: view)
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailTextField.text = ""
        passwordTextField.text = ""
        
        keyboardNotificationHandler.subscribeToKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        
        keyboardNotificationHandler.unSubscribeFromKeyboardNotifications()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.tabBarItem = UITabBarItem(title: "Профиль", image: #imageLiteral(resourceName: "profile"), tag: 1)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: Setup
extension UserRegisterController {
    
    func activeField() -> UIView? {
        return [emailTextField, passwordTextField].first(where: {$0.isFirstResponder})
    }
    
    func setupView() {
        
        if self.tabBarController?.tabBar.selectedItem?.tag == 1 {
            miss.isHidden = true
        }
        
        emailTextField.nextField = passwordTextField

        setupKeyboardNotificationHandler()
        setupButton()
        self.navigationItem.title = "Login"
    }
    
    func setupKeyboardNotificationHandler() {
        keyboardNotificationHandler.setup(withView: view,
                                          scrollView: scrollView,
                                          activeFrameGetter: self)
    }
    
    func setupButton() {
        entry.layer.cornerRadius = 8.0
        register.layer.cornerRadius = 8.0
        miss.layer.cornerRadius = 8.0
    }
}


//MARK: Action
extension UserRegisterController {
    
    @IBAction func entryPress(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        guard let email = emailTextField.text, let password = passwordTextField.text, email != "", password != "" else {
            self.showAlertError(mesageTitle: "Error", message: "Неверный логин или пароль")
            return
        }
        
        UserRequestManager.instance.loginUser(email: email, password: password, success: { [weak self] user in
            
            guard let strongSelf = self else {
                return
            }
            
            AppSettings.sharedInstance.userId = user.id
            AppSettings.sharedInstance.userAvatar = user.avatar_url

            AppSettings.sharedInstance.attributeSave(newValue: user.firstname, keyType: .firstname)
            AppSettings.sharedInstance.attributeSave(newValue: user.lastname, keyType: .lastname)
            AppSettings.sharedInstance.attributeSave(newValue: user.email, keyType: .email)
            AppSettings.sharedInstance.attributeSave(newValue: user.password, keyType: .password)
            
            if let tc = strongSelf.tabBarController as? TabBarController {
                tc.reloadControllers()
            } else {
                strongSelf.present(TabBarController(), animated: true, completion: nil)
            }
        }) { [weak self] (error) in
            self?.showAlertError(mesageTitle: "Error", message: error)
        }
    }
    
    @IBAction func registerPress(_ sender: UIButton) {
        self.navigationController?.pushViewController(RegisterController(), animated: true)
    }
    
    
    @IBAction func missPress(_ sender: UIButton) {
        self.present(TabBarController(), animated: true, completion: nil)
    }
}

// MARK: UITextFieldDelegate
extension UserRegisterController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _ = textField.resignFirstResponder()
        _ = (textField as? TextField)?.nextField?.becomeFirstResponder()
        return true
    }
}

