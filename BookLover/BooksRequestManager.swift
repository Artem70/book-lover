//
//  LoadJson.swift
//  BookLover
//
//  Created by Artem Pashkevich on 17.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BookManager {
    
    //TODO: Server is not ready :(
    
//    
//    // Singleton
//    static let instance = BookManager()
//    
//    private init() {}
//    
//    var loadJSONproperties: ((Bool),(String)) -> Void = {_ in}
//    
//    func loadJSON() {
//    
//        let url = "http://private-b800a9-booklover.apiary-mock.com/init.php/api/books"
//        
//        Alamofire.request(url, method: .get).validate().responseJSON { response in
//            
//            switch response.result {
//                
//            case .success(let value):
//                let json = JSON(value)
//                
//                for objJSON in json.array! {
//                
//                    CoreDataStack.instance.parsingJSON(name: objJSON["name"].stringValue, author: objJSON["author"].stringValue, whoAdvised: objJSON["who_advise"].stringValue, rating: objJSON["rating"].floatValue, searchid: objJSON["id"].int16Value)
//                    
//                    self.loadJSONproperties(true, "")
//                    
//                }
//            case .failure(let error):
//                print(error)
//                self.loadJSONproperties(false, "\(error)")
//            }
//        }
//    }
//    
//    
//    func createBook(_ book: BookData) {
//        
//        Alamofire.request("https://private-b800a9-booklover.apiary-mock.com/books", method: .post, parameters: book.parameters()).validate().responseJSON { response in
//
//            switch response.result {
//                
//            case .success(let value):
//                let json = JSON(value)
//        
//                var objJSON = json
//                
//              CoreDataStack.instance.saveDataJSON(name: objJSON["name"].stringValue, author: objJSON["author"].stringValue, whoAdvised: objJSON["who_advise"].stringValue, rating: objJSON["rating"].floatValue, id: objJSON["id"].int16Value)
//                
//                self.loadJSONproperties(true, "")
//                
//            case .failure(let error):
//                print("Request failed with error: \(error)")
//                self.loadJSONproperties(false, "\(error)")
//                
//            }
//        }
//    }
//    
//    
//    func updateBook(_ book: BookData) {
//        
//        Alamofire.request("https://private-b800a9-booklover.apiary-mock.com/books", method: .put, parameters: book.parameters()).validate().responseJSON { response in
//          
//            switch response.result {
//                
//            case .success(let value):
//                let json = JSON(value)
//                
//                var objJSON = json
//                
//                CoreDataStack.instance.parsingJSON(name: objJSON["name"].stringValue, author: objJSON["author"].stringValue, whoAdvised: objJSON["who_advise"].stringValue, rating: objJSON["rating"].floatValue, searchid: objJSON["id"].int16Value)
//                
//                self.loadJSONproperties(true, "")
//                
//            case .failure(let error):
//                print("Request failed with error: \(error)")
//                self.loadJSONproperties(false, "\(error)")
//                
//            }
//        }
//    }
//    
//    func deleteBook(_ id: Any) {
//        
//        Alamofire.request("https://private-b800a9-booklover.apiary-mock.com/books", method: .delete, parameters: ["id" : id]).validate().responseJSON { response in
//            
//            switch response.result {
//                
//            case .success(let value):
//                let json = JSON(value)
//                
//                let objJSON = json
//                
//                print(objJSON)
//                
//                self.loadJSONproperties(true, "")
//                
//            case .failure(let error):
//                print("Request failed with error: \(error)")
//                self.loadJSONproperties(false, "\(error)")
//                
//            }
//        }
//    }
}



