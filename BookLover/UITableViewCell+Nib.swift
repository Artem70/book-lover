//
//  UITableViewCell+Nib.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static func nib() -> UINib {
        let name = NSStringFromClass(self)
        
        let arrayName = name.components(separatedBy: ".")
        
        guard let clearName = arrayName.last else {
            fatalError()
        }
        
        return UINib(nibName: clearName, bundle: Bundle.main)
    }
}

