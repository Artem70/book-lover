//
//  ProfileController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 09.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD

class ProfileController: BaseController, ActiveFieldFrameGetter{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!

    fileprivate var objectsArray: [DataStruct] = []
    
    struct DataStruct {
        let type: DataType
        var value: String?
    }
    
    enum DataType: Int {
        case Firstname
        case Lastname
        case Email
        case Password
        
        func stringValue() -> String {
            switch self {
            case .Firstname:
                return "Имя"
            case .Lastname:
                return "Фамилия"
            case .Email:
                return "Email"
            case .Password:
                return "Пароль"
            }
        }
        
        func keyboard() -> UIKeyboardType {
            switch self {
            case .Firstname, .Lastname, .Password:
                return UIKeyboardType.default
            case .Email:
                return UIKeyboardType.numbersAndPunctuation
            }
        }
    }
    
    fileprivate var keyboardNotificationHandler: KeyboardNotificationHandler = KeyboardNotificationHandler()
    
    var activeFieldViewFrame: CGRect? {
        return tableView.visibleCells.first(where: { $0.isFirstResponder })?.frame
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
        
        keyboardNotificationHandler.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        
        keyboardNotificationHandler.unSubscribeFromKeyboardNotifications()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.tabBarItem = UITabBarItem(title: "Профиль", image: #imageLiteral(resourceName: "profile"), tag: 1)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//MARK: Setup
extension ProfileController {
    
    func setupView() {
        
        setupButton()
        setupNavigationItem()
        setupTableView()
    }
    
    func setupNavigationItem() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(saveButtonPressed(_:)))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Выйти",
            style: .plain,
            target: self,
            action: #selector(exitButtonPressed(_:)))
        
        self.navigationItem.title = "Профиль"
    }
    
    func setupButton() {
        button.addTarget(self, action: #selector(photoButtonPresed(_:)), for: .touchUpInside)
        button.clipsToBounds = true
        button.layer.cornerRadius = button.bounds.size.width/2
        button.layer.masksToBounds = true
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 5.0
    }
    
    func setupTableView() {
        tableView.register(BookCell.nib(), forCellReuseIdentifier: BookCell.reuseIdentifier())
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.keyboardDismissMode = .onDrag
        
        keyboardNotificationHandler.setup(withView: view,
                                          scrollView: tableView,
                                          activeFrameGetter: self)
        
    }
}

//MARK: LoadData
extension ProfileController {
    
    func loadData() {
        
        objectsArray = [
            DataStruct(type: .Firstname, value: AppSettings.sharedInstance.attributeSet(keyType: .firstname)),
            DataStruct(type: .Lastname, value: AppSettings.sharedInstance.attributeSet(keyType: .lastname)),
            DataStruct(type: .Email, value:  AppSettings.sharedInstance.attributeSet(keyType: .email)),
            DataStruct(type: .Password, value: AppSettings.sharedInstance.attributeSet(keyType: .password))
        ]
        
        let url = AppSettings.sharedInstance.userAvatar
        
        guard let id = AppSettings.sharedInstance.userId else {
            print("Error user_id = nil")
            return
        }
        
        //load image
        if AppSettings.sharedInstance.attributeSetImage(key: id) != nil {
            
            button.setImage(url: "\(id)")
            
        } else if url != nil && url != "" {
            
            HUD.show(.progress, onView: button)
            
            button.imageView?.imageFromUrl(urlString: url, completionHandler: { (image) in
                self.button.setImage(image, for: .normal)
    
                FileManager.saveImageDocumentDirectory(image: image!, avatar_url: "\(id)")
                HUD.hide(animated: true)
            })
            AppSettings.sharedInstance.attributeSaveImage(newValue: url!, key: id)
            
        } else {
            button.setImage(#imageLiteral(resourceName: "profileImage"), for: .normal)
        }
    }
}

//MARK: Action
extension ProfileController {
    
    @objc func saveButtonPressed(_ sender: UIBarButtonItem) {
        
        self.view.endEditing(true)
        
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        self.tableView.tableHeaderView?.isHidden = false
        
        let imageData = button.imageView?.image
        
        guard let data = UIImageJPEGRepresentation(imageData!, 1) else {
            print("What is it?")
            return
        }
        
        self.tableView.tableHeaderView?.isHidden = false
        
        var isValid = true
        for obj in objectsArray {
            if obj.value == nil || obj.value == "" {
                isValid = false
                break
            }
        }
        
        if isValid {
            
            var firstName = ""
            var lastName = ""
            var email = ""
            var password = ""
            
            for bookObj in self.objectsArray {
                
                switch bookObj.type {
                case .Firstname:
                    firstName = bookObj.value!
                case .Lastname:
                    lastName = bookObj.value!
                case .Email:
                    email = bookObj.value!
                case .Password:
                    password = bookObj.value!
                    
                }
            }
            
            let user = UserDataSend(first: firstName, last: lastName, email:  email, password: password, id: AppSettings.sharedInstance.userId!)

            UserRequestManager.instance.updateUser(user, imageData: data, success: { userResponse in
                
                AppSettings.sharedInstance.attributeSave(newValue: userResponse.firstname, keyType: .firstname)
                AppSettings.sharedInstance.attributeSave(newValue: userResponse.lastname, keyType: .lastname)
                AppSettings.sharedInstance.attributeSave(newValue: userResponse.email, keyType: .email)
                AppSettings.sharedInstance.attributeSave(newValue: userResponse.password, keyType: .password)
                
                FileManager.saveImageDocumentDirectory(image: imageData!, avatar_url: "\(userResponse.id!)")
                
            }) { [weak self] (error) in
                
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.showAlertError(mesageTitle: "Error", message: error)
            }
        } else {
            self.showAlertError(mesageTitle: "Заполните все поля", message: "Попробуйте еще раз")
        }
    }
    
    //MARK: exitButton
    @objc func exitButtonPressed(_ sender: UIBarButtonItem) {
        
        if let id = AppSettings.sharedInstance.userId  {
            
            UserRequestManager.instance.logOut(userId: id, success: { status in
                
                print(status)
                
            }){ [weak self] (error) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.showAlertError(mesageTitle: "Error", message: error)
            }
        }
        
        if let pc = presentingViewController {
            pc.dismiss(animated: true, completion: nil)
        } else {
            UIApplication.shared.keyWindow?.rootViewController = NavBarController.createNavBar(viewController: UserRegisterController())
        }
        
        //BookManager.instance.posts.removeAll()
        AppSettings.sharedInstance.deleteObject()
        tableView.reloadData()
    }
}


//MARK: UITableViewDataSource, UITableViewDelegate
extension ProfileController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.reuseIdentifier(), for: indexPath) as! BookCell
        
        cell.nameLabel.text = objectsArray[indexPath.row].type.stringValue()
        cell.textField.tag = objectsArray[indexPath.row].type.rawValue
        cell.textField.keyboardType = objectsArray[indexPath.row].type.keyboard()
        cell.textField.text = objectsArray[indexPath.row].value
        cell.textField.delegate = self
        cell.selectionStyle = .none
        
        if indexPath.row == 3 {
            cell.textField.isSecureTextEntry = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray.count
    }
}

//MARK: UITextFieldDelegate
extension ProfileController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        objectsArray[textField.tag].value = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: Photo PickerController
extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        button.setImage(info [UIImagePickerControllerEditedImage] as? UIImage, for: .normal)
        button.contentMode = .scaleAspectFit
        button.clipsToBounds = true
        dismiss(animated: true, completion: nil)
        
    }
    
    @objc func photoButtonPresed(_ sender: UIButton) {
        
        let alertcontroller = UIAlertController(title: "Источник фотографии", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (action) in
            self.chooseImagePickerAction(source: .camera)
        }
        let photoLibraryAction = UIAlertAction(title: "Фото(библиотека)", style: .default) { (action) in self.chooseImagePickerAction(source: .photoLibrary)
            
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alertcontroller.addAction(cameraAction)
        alertcontroller.addAction(photoLibraryAction)
        alertcontroller.addAction(cancelAction)
        
        self.present(alertcontroller, animated: true, completion: nil)
    }
    
    func chooseImagePickerAction(source: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            self.present(imagePicker, animated: true, completion: nil )
        }
    }
}
