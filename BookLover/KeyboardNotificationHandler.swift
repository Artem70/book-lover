//
//  KeyboardNotificationHandler.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit

protocol ActiveFieldFrameGetter {
    var activeFieldViewFrame: CGRect? { get }
}

class KeyboardNotificationHandler {
    
    var view: UIView!
    var scrollView: UIScrollView!
    var activeFrameGetter: ActiveFieldFrameGetter!
    
    var notificationCenter: NotificationCenter {
        return NotificationCenter.default
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    func setup(withView view: UIView,
               scrollView: UIScrollView,
               activeFrameGetter: ActiveFieldFrameGetter) {
        
        self.view = view
        self.scrollView = scrollView
        self.activeFrameGetter = activeFrameGetter
        
        configure()
    }
    
    func subscribeToKeyboardNotifications() {
        
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardDidShow(notification:)),
                                       name: .UIKeyboardDidShow,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillHide(notification:)),
                                       name: .UIKeyboardWillHide,
                                       object: nil)
    }
    
    func unSubscribeFromKeyboardNotifications() {
        notificationCenter.removeObserver(self, name: .UIKeyboardDidShow, object: nil)
        notificationCenter.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
}

//MARK: Notifications
private extension KeyboardNotificationHandler {
    
    @objc func keyboardDidShow(notification: Notification) {
        
        guard let userInfo = notification.userInfo else {
            return
        }
        
        guard let keyboardFrameValue = userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue,
            let activeFieldRect = activeFrameGetter.activeFieldViewFrame else {
                return
        }
        
        let keyboardSize = keyboardFrameValue.cgRectValue.size
        var contentInsets = scrollView.contentInset
        contentInsets.bottom = keyboardSize.height
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        var rect = scrollView.frame
        rect.size.height -= keyboardSize.height
        if !rect.contains(activeFieldRect) {
            scrollView.scrollRectToVisible(activeFieldRect, animated: true)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        var contentInsets = scrollView.contentInset
        contentInsets.bottom = 0.0
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
}

//MARK: Actions
private extension KeyboardNotificationHandler {
    @objc func viewTapped() {
        view.endEditing(true)
    }
}

//MARK: Supporting methods
private extension KeyboardNotificationHandler {
    func configure() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
}
