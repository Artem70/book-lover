//
//  UITableViewCell+ReuseIdentifier.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static func reuseIdentifier() -> String {
        
        let name = NSStringFromClass(self)
        let arrayName = name.components(separatedBy: ".")
        guard let clearName = arrayName.last else {
            fatalError()
        }
        
        return clearName
    }
}

