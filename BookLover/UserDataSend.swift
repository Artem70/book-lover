//
//  UserDataSend.swift
//  BookLover
//
//  Created by Artem Pashkevich on 13.09.17.
//  Copyright © 2017 ASDGroup. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct UserDataSend {
    
    var email: String
    var password: String
    var lastname: String
    var firstname: String
    var id: String
    var avatar_url: String?
    
    init (first: String, last: String, email: String, password: String, id: String) {
        
        self.email = email
        self.password = password
        self.lastname = first
        self.firstname = last
        self.id = id        
    }
    
    init?(json: [String: Any]) {
        
        guard
            let email = json["email"] as? String,
            let password = json["password"] as? String,
            let lastname = json["lastname"] as? String,
            let firstname = json["firstname"] as? String,
            let id = json["id"] as? String
            else {
                return nil
        }
        
        self.avatar_url = json["avatar_url"] as? String
        self.email = email
        self.password = password
        self.lastname = lastname
        self.firstname = firstname
        self.id = id
    }
    
    func updateParameters() -> [String: Any] {
        let params = [
            "email": email,
            "password": password,
            "lastname": firstname,
            "firstname": lastname,
            "user_id": id
        ]
        return params
    }
    
    
    static func getArray(json: JSON) -> UserDataSend? {
        
        let user: UserDataSend
        let objJSON = json["data"]["user"].dictionaryObject
        guard let dictionaryJSON = objJSON else {
            print("Error dictionaryJSON")
            return nil
        }
        
        guard let post = UserDataSend(json: dictionaryJSON) else { return nil}
        user = post
        
        return user
    }
}
