//
//  BookDetailController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 9.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import CoreData
import FloatRatingView


protocol BookDetailControllerDelegate {
    func detailControllerDidChangeBook(_ book: Book)
}

class BookDetailController: BaseController, ActiveFieldFrameGetter {
    
    var delegate: BookDetailControllerDelegate?
    var book: Book!
    var objects: [BookProperties] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    struct BookProperties {
        var type: DataType
        var value: Any?
    }
    
    enum DataType: Int {
        case Name
        case Author
        case WhoAdvised
        case Readed
        case Rating
        
        func stringValue() -> String {
            switch self {
            case .Name:
                return "Название"
            case .Author:
                return "Автор"
            case .WhoAdvised:
                return "Кто посоветовал"
            case .Readed:
                return "Прочитал?"
            case .Rating:
                return "Моя оценка"
            }
        }
        
        var showContactButton: Bool {
            switch self {
            case .WhoAdvised:
                return true
            default:
                return false
            }
        }
    }
    
    enum State {
        case normal
        case editing
        
        func rightButtonType() -> UIBarButtonSystemItem {
            switch self {
            case .normal:
                return .edit
            case .editing:
                return .save
            }
        }
        
        func textFieldsEditable() -> Bool {
            switch self {
            case .normal:
                return false
            case .editing:
                return true
            }
        }
    }
    
    fileprivate var keyboardNotificationHandler: KeyboardNotificationHandler = KeyboardNotificationHandler()
    
    var activeFieldViewFrame: CGRect? {
        return tableView.visibleCells.first(where: { $0.isFirstResponder })?.frame
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardNotificationHandler.subscribeToKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        keyboardNotificationHandler.unSubscribeFromKeyboardNotifications()
    }
    
    var currentState: State = .normal {
        didSet {
            applyNewState()
        }
    }
}

//MARK: SetupView
extension BookDetailController {
    
    func setupView() {
        applyNewState()
        setupTableView()

        self.navigationItem.title = "Детальная информация"
        
        keyboardNotificationHandler.setup(withView: view,
                                          scrollView: tableView,
                                          activeFrameGetter: self)
    }
    
    func applyNewState() {
        
        let button = UIBarButtonItem(barButtonSystemItem: currentState.rightButtonType(),
                                     target: self,
                                     action: #selector(rightButtonPressed(_:)))
        
        navigationItem.rightBarButtonItem = button
        
        tableView.reloadData()
    }
    
    func setupTableView() {
        
        tableView.register(BookCell.nib(), forCellReuseIdentifier: BookCell.reuseIdentifier())
        tableView.register(ReadBookCell.nib(), forCellReuseIdentifier: ReadBookCell.reuseIdentifier())
        tableView.register(RatingCell.nib(), forCellReuseIdentifier: RatingCell.reuseIdentifier())
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.keyboardDismissMode = .onDrag
    }
}

//MARK: Action
extension BookDetailController {
    
    @objc func rightButtonPressed(_ sender: UIBarButtonItem) {
        
        switch self.currentState {
        case .normal:
            currentState = .editing
        case .editing:
            view.endEditing(true)
            saveBook()
            currentState = .normal
        }
    }
    
    func saveBook() {
        
        var nameObj = ""
        var authorObj = ""
        var whoAdvisedObj = ""
        
        for bookObj in objects {
            
            switch bookObj.type {
            case .Name:
                nameObj = (bookObj.value as? String)!
            case .Author:
                authorObj = (bookObj.value as? String)!
            case .WhoAdvised:
                whoAdvisedObj = (bookObj.value as? String)!
            default:
                break
            }
        }
        book.name = nameObj
        book.author = authorObj
        book.whoAdvised = whoAdvisedObj
        
        do {
            try book.managedObjectContext?.save()
        } catch {
            print(error.localizedDescription)
        }
        delegate?.detailControllerDidChangeBook(book)
    }
}

//MARK: Load Data
extension BookDetailController {
    
    func loadData() {
        objects = [
            BookProperties(type: .Name, value: book.name!),
            BookProperties(type: .Author, value: book.author!),
            BookProperties(type: .WhoAdvised, value: book.whoAdvised!),
            BookProperties(type: .Readed, value: book.readBook),
            BookProperties(type: .Rating, value: nil)
        ]
    }
}

//MARK: BookCellDelegate
extension BookDetailController: BookCellDelegate {
    
    func bookCell(cell: BookCell, buttonPressed: UIButton) {
        
        self.view.endEditing(true)
        
        if currentState == .editing {
            let vc = ContactsController()
            vc.contactsDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK: ContactsControllerDelegate
extension BookDetailController: ContactsControllerDelegate {
    
    func contactsValue(value: String) {
        var contactObj = objects.first(where: { $0.type == .WhoAdvised })
        contactObj?.value = value
        tableView.reloadData()
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate
extension BookDetailController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = objects[indexPath.row]
        
        switch obj.type {
            
        case .Name, .Author, .WhoAdvised:
            let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.reuseIdentifier(), for: indexPath) as! BookCell
            let obj = objects[indexPath.row]
            
            if currentState.textFieldsEditable() {
                
                cell.textField.borderStyle = .roundedRect
                cell.showContactsButton = obj.type.showContactButton
            } else {
                cell.textField.borderStyle = .none
                cell.showContactsButton = false
            }
            
            cell.nameLabel.text = obj.type.stringValue()
            cell.textField.textAlignment = .right
            cell.textField.text = obj.value as? String
            cell.textField.delegate = self
            cell.textField.isUserInteractionEnabled = currentState.textFieldsEditable()
            cell.textField.tag = indexPath.row
            cell.delegateBookCell = self
            cell.selectionStyle = .none
            
            return cell
            
        case .Readed:
            
            let cellRead = tableView.dequeueReusableCell(withIdentifier: ReadBookCell.reuseIdentifier(), for: indexPath) as! ReadBookCell
            
            cellRead.readLabel.text = objects[indexPath.row].type.stringValue()
            
            if obj.value as? Bool == true {
                cellRead.imageRead.image = #imageLiteral(resourceName: "check_on")
            } else if obj.value as? Bool == false{
                cellRead.imageRead.image = #imageLiteral(resourceName: "check_off")
            }
            return cellRead
            
        case .Rating:
            
            let cellRating = tableView.dequeueReusableCell(withIdentifier: RatingCell.reuseIdentifier(), for: indexPath) as! RatingCell
            
            cellRating.ratingLabel.text = objects[indexPath.row].type.stringValue()
            cellRating.floatRatingView.delegate = self
            cellRating.floatRatingView.rating = book.rating
            cellRating.selectionStyle = .none
            
            return cellRating
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true )
        
        let obj = objects[indexPath.row]
        
        switch obj.type {
            
        case .Name, .Author , .WhoAdvised:
            
            if currentState.textFieldsEditable() {
                
                let cell = tableView.cellForRow(at: indexPath) as! BookCell
                
                cell.textField.becomeFirstResponder()
            }
            
        case .Readed:
            
            let cellRead = tableView.cellForRow(at: indexPath) as! ReadBookCell
            
            if cellRead.imageRead.image == #imageLiteral(resourceName: "check_off") {
                
                cellRead.imageRead.image = #imageLiteral(resourceName: "check_on")
                
                book.readBook = true
                
                do {
                    try book.managedObjectContext?.save()
                } catch {
                    print(error.localizedDescription)
                }
            } else if cellRead.imageRead.image == #imageLiteral(resourceName: "check_on"){
                
                cellRead.imageRead.image = #imageLiteral(resourceName: "check_off")
                
                book.readBook = false
                
                //Save context
                do {
                    try book.managedObjectContext?.save()
                } catch {
                    print(error.localizedDescription)
                }
            }
            delegate?.detailControllerDidChangeBook(book)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
}


//MARK: FloatRatingViewDelegate
extension BookDetailController: FloatRatingViewDelegate{
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float) {
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        
        book.rating = rating
        
        do {
            try book.managedObjectContext?.save()
        } catch {
            print(error.localizedDescription)
        }
        
        self.delegate?.detailControllerDidChangeBook(book)
    }
}

//MARK: UITextFieldDelegate
extension BookDetailController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        objects[textField.tag].value = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //Allow edit
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return currentState.textFieldsEditable()
    }
}


