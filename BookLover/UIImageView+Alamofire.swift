//
//  UIImageView+Alamofire.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Alamofire

extension UIImageView {
    
    public func imageFromUrl(urlString: String?) {
        imageFromUrl(urlString: urlString) { (image) in
            self.image = image
        }
    }
    
    public func imageFromUrl(urlString: String?, completionHandler: @escaping (UIImage?) -> Void) {
        UIImageView.imageFromUrl(urlString: urlString, completionHandler: completionHandler)
    }
    
    public static func imageFromUrl(urlString: String?, completionHandler: @escaping (UIImage?) -> Void) {
        
        guard let validUrlString = urlString else {
            completionHandler(nil)
            return
        }
        guard let url = URL(string: validUrlString) else {
            completionHandler(nil)
            return
        }
        Alamofire.request(url, method: .get, parameters: nil).responseData(queue: DispatchQueue.main) { (response) in
            
            if let data = response.data {
                completionHandler(UIImage(data: data))
            } else {
                completionHandler(nil)
            }
        }
    }
}
