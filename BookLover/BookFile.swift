//
//  BookFile.swift
//  BookLover
//
//  Created by Artem Pashkevich on 17.07.17.
//  Copyright © 2017 ASDGroup. All rights reserved.
//

import Foundation


class Temp: Book {
    
    public var author: String?
    public var name: String?
    public var readBook: Bool
    public var whoAdvised: String?
    public var rating: Float

}
