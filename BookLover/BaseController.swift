//
//  BaseController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08/09/2017.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class BaseController: UIViewController {
    
    enum BackgroundStyle {
        case lightGray
        case white
        
        func color() -> UIColor {
            switch self {
            case .lightGray:
                return UIColor.lightGray
            case .white:
                return UIColor.white
            }
        }
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func preferredBackgroundStyle() -> BackgroundStyle {
        return .lightGray
    }
}
