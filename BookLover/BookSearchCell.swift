//
//  BookSearchCell.swift
//  BookLover
//
//  Created by Artem Pashkevich on 10.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class BookSearchCell: UITableViewCell {
    
    var bookSearch: Book? {
        didSet {
            applyData()
        }
    }

    @IBOutlet weak var bookNameSearch: UILabel!
    @IBOutlet weak var bookAuthorSearch: UILabel!
    @IBOutlet weak var descriptionSearch: UILabel!
    @IBOutlet weak var readedBookSearch: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        readedBookSearch.textColor = AppColor.greenColor()
        // Initialization code
    }

    private func applyData() {
        
        guard let searchValue = bookSearch else {
            return
        }
        bookNameSearch.text = searchValue.name
        bookAuthorSearch.text = searchValue.author
        descriptionSearch.text = searchValue.whoAdvised
        
        if searchValue.readBook {
            readedBookSearch.text = "Прочитано"
        } else {
            readedBookSearch.text = ""
        }
    }
}
