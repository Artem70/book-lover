//
//  BookData.swift
//  BookLover
//
//  Created by Artem Pashkevich on 21.07.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

//TODO: Server is not ready :(

struct BookData {
    var name: String
    var author: String
    var who_advise: String
    //var rating: String
    //var id: String
    
    init (name: String, author: String, who_advise: String) {
        
        self.name = name
        self.author = author
        self.who_advise = who_advise
        //self.rating = rating
        //self.id = id
        
    }
    
    func parameters() -> [String: Any] {
        let params = [
            "name": name,
            "author": author,
            "who_advise": who_advise
            //"rating": rating,
           // "id": id
        ] as [String : Any]
        return params
    }
}
