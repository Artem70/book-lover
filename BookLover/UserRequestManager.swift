//
//  UserRequestManager.swift
//  BookLover
//
//  Created by Artem Pashkevich on 11.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UserRequestManager {
    
    // Singleton
    static let instance = UserRequestManager()
    
    private init() {}
    
    var properties: ((Bool), (String), (String)) -> Void = {_ in}
    var user: UserData?
    static let baseUrl =  "http://booklover.asdgroup.pro/init.php/api"
    
    
    private func requestManager (path: String, method: HTTPMethod, parameters: [String: Any]) -> DataRequest {
        return Alamofire.request("\(UserRequestManager.baseUrl)/\(path)", method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate()
    }
    
    func createUser(_ user: UserData, imageData: Data,
                    success: @escaping ((UserData) -> Void),
                    failed: @escaping ((String) -> Void)) {
        
        requestManager(path: "register", method: .post, parameters: user.parameters()).responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                
                let objJSON = json["data"]["user"].dictionaryObject
                
                guard let dictionaryJSON = objJSON else {
                    print("Error dictionaryJSON")
                    return
                }
                
                //func get
                guard let post = UserData(json: dictionaryJSON) else { return }
                
                self.user = post
                
                //Save image to server
                self.avatarSave(imageData, ext_id: dictionaryJSON["id"] as! String)
                
                success(post)
                print(self.user ?? "user JSON not loading")
                
            case .failure(let error):
                failed("Request failed with error: \(error)")
            }
        }
    }
    
    
    func updateUser(_ user: UserDataSend, imageData: Data,
                    success: @escaping ((UserData) -> Void),
                    failed: @escaping ((String) -> Void)) {
        
        requestManager(path: "edituser", method: .put, parameters: user.updateParameters()).responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                
                let objJSON = json["data"].dictionaryObject
                
                guard let dictionaryJSON = objJSON else {
                    print("Error dictionaryJSON")
                    return
                }
                
                //func get
                guard let post = UserData(json: dictionaryJSON) else { return }
                self.user = post
                
                //Save image to server
                self.avatarSave(imageData, ext_id: dictionaryJSON["id"] as! String)
                
                success(post)
                
                print(self.user ?? "user JSON not loading")
                
            case .failure(let error):
                failed("Request failed with error: \(error)")
            }
        }
    }
    
    func loginUser(email: Any, password: Any,
                   success: @escaping ((UserData) -> Void),
                   failed: @escaping ((String) -> Void)) {
        
        //URLEncoding.default
        Alamofire.request("\(UserRequestManager.baseUrl)/login", method: .get, parameters: ["email": email, "password": password]).validate().responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                self.user = UserData.getArray(json: json)
                
                if self.user == nil {
                    failed("No such user")
                    return
                }
                success(self.user!)
                
            case .failure(let error):
                failed("Request failed with error: \(error)")
                print(error)
            }
        }
    }
    
    
    func logOut(userId: Any,
                success: @escaping ((String) -> Void),
                failed: @escaping ((String) -> Void)) {
        
        Alamofire.request("\(UserRequestManager.baseUrl)/logout", method: .get, parameters: ["email": userId]).validate().responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                let status = json["status"].stringValue
                success(status)
                
            case .failure(let error):
                failed("Request failed with error: \(error)")
                print(error)
            }
        }
    }
    
    //MARK: Avatar Save
    func avatarSave(_ imageData: Data, ext_id: String) {
        
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        
        requestManager(path: "uploadavatar", method: .post, parameters: ["avatar": strBase64, "user_id" : ext_id]).responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                let avatarJSON = json["data"]["url"].stringValue
                self.properties(true, "", avatarJSON)
                
            case .failure(let error):
                self.properties(false, "Request failed with error: \(error)", "")
                print("Request failed with error: \(error)")
            }
        }
    }
}







