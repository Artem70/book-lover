//
//  AppSettings.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

class AppSettings {
    
    enum keys: String {
        case userIdKey = "dwafmwpfnweoi"
        case email = "email"
        case password = "pasword"
        case avatar_url = "avatar_url"
        case firstname = "firstname"
        case lastname = "lastname"
    }
    
    //MARK: Internal propeties
    static let sharedInstance = AppSettings()
    
    var userId : String? {
        get {
            return defaults.object(forKey: keys.userIdKey.rawValue) as? String
        }
        set {
            defaults.set(newValue, forKey: keys.userIdKey.rawValue)
        }
    }
    
    var userAvatar : String? {
        get {
            return defaults.object(forKey: keys.avatar_url.rawValue) as? String
        }
        set {
            defaults.set(newValue, forKey: keys.avatar_url.rawValue)
        }
    }
    
    //MARK: Attribute Value
    
    func attributeSave(newValue: String?,keyType: keys) {
        guard let value = newValue else {
            print("attribute UserDefaults = nil")
            return
        }
        defaults.set(value, forKey: keyType.rawValue)
    }
    
    func  attributeSet(keyType: keys) -> String {
        return (defaults.object(forKey: keyType.rawValue) as? String)!
    }
    
    
    //MARK: Attribute Image
    func attributeSaveImage(newValue: String?,key: String) {
        
        guard let value = newValue else {
            print("attribute UserDefaults = nil")
            return
        }
        defaults.set(value, forKey: key)
    }
    
    func attributeSetImage(key: String) -> String? {
        return defaults.object(forKey: key) as? String
    }
    
    func deleteObject() {
        defaults.removeObject(forKey: keys.userIdKey.rawValue)
    }
    
    //MARK: Private properties
    fileprivate let defaults = UserDefaults.standard
    
    
    private init() {
        
    }
}
