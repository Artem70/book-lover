//
//  BookListCell.swift
//  BookLover
//
//  Created by Artem Pashkevich on 09.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import FloatRatingView

class BookListCell: UITableViewCell {

    var book: Book? {
        didSet {
            applyData()
        }
    }
    
    @IBOutlet fileprivate weak var bookNameLabel: UILabel!
    
    @IBOutlet fileprivate weak var bookAuthorLabel: UILabel!
    
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!
    
    @IBOutlet fileprivate var floatRating: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Required float rating view params
        floatRating.emptyImage = #imageLiteral(resourceName: "StarEmpty")
        floatRating.fullImage = #imageLiteral(resourceName: "StarFull")
        // Optional params
        floatRating.contentMode = UIViewContentMode.scaleAspectFit
        floatRating.maxRating = 5
        floatRating.minRating = 0
        floatRating.editable = true
        floatRating.halfRatings = true
        floatRating.floatRatings = false

        
        // Initialization code
    }

    private func applyData() {
        guard let validBook = book else {
            return
        }
        
        bookNameLabel.text = validBook.name
        bookAuthorLabel.text = validBook.author
        descriptionLabel.text = validBook.whoAdvised
        floatRating.rating = validBook.rating
    }
}
