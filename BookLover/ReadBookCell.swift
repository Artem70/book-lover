//
//  ReadBookCell.swift
//  BookLover
//
//  Created by Artem Pashkevich on 11.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit


protocol ReadBookCellDelegate: class {
    
    func cellRead(cell: ReadBookCell, buttonPressed: UIButton)
}

class ReadBookCell: UITableViewCell {

    @IBOutlet weak var imageRead: UIImageView!
    @IBOutlet weak var buttonOutlet: UIButton!
    @IBOutlet weak var readLabel: UILabel!
 
    weak var delegate: ReadBookCellDelegate?
}

//MARK: Actions
private extension ReadBookCell {
    
    @IBAction func tapOnCheckButton(_ sender: UIButton) {
        
        delegate?.cellRead(cell: self, buttonPressed: sender)
    }
}
