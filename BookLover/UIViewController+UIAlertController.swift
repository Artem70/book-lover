//
//  File.swift
//  BookLover
//
//  Created by Artem Pashkevich on 17.09.17.
//  Copyright © 2017 ASDGroup. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func showAlertError(mesageTitle: String, message: String) {
        let alertERROR = UIAlertController(title: mesageTitle , message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok" , style: .cancel, handler: nil)
        alertERROR.addAction(okAction)
        alertERROR.setValue(NSAttributedString(string: mesageTitle, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 20),NSForegroundColorAttributeName : UIColor.red]), forKey: "attributedTitle")
        self.present(alertERROR, animated: true, completion: nil)
    }
    
    func showSuccessAlert (message: String, messageTitle: String) {
        let alert = UIAlertController(title: messageTitle , message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok" , style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.setValue(NSAttributedString(string: message, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 16),NSForegroundColorAttributeName : UIColor.darkText]), forKey: "attributedMessage")
        alert.setValue(NSAttributedString(string: messageTitle, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 20),NSForegroundColorAttributeName : UIColor(displayP3Red: 72/255.0, green: 146/255.0, blue: 33/255.0, alpha: 1.0)]), forKey: "attributedTitle")
        self.present(alert, animated: true, completion: nil)
    }

}
