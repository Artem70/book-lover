//
//  ContactsCell.swift
//  BookLover
//
//  Created by Artem Pashkevich on 10.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import Contacts

class ContactsCell: UITableViewCell {

    var contacts: CNContact? {
        didSet {
            applyData()
        }
    }
    
    @IBOutlet weak var imageContacts: UIImageView!
    
    @IBOutlet weak var nameContacts: UILabel!
    
    @IBOutlet weak var secondNameContacts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func applyData() {
        guard let validContacts = contacts else {
            return
        }
        
        nameContacts.text = validContacts.givenName
        secondNameContacts.text = validContacts.familyName
        
        if let imageData = validContacts.thumbnailImageData {
            imageContacts.image = UIImage(data: imageData)
        } else {
            imageContacts.image = #imageLiteral(resourceName: "avatar")
        }
    }

}
