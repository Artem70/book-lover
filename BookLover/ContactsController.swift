//
//  ContactsController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 12.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import Contacts


protocol ContactsControllerDelegate: class {
    
    func contactsValue(value: String)
}

class ContactsController: BaseController {

    @IBOutlet weak var tableView: UITableView!
    var searchController: UISearchController!
    weak var contactsDelegate: ContactsControllerDelegate?
    var contactsArray: [CNContact] = []
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        reloadDataContacts()
    }
}


//MARK:Load Data
extension ContactsController {
    
    func filterContentFor(searchText text: String) {
        let filteredResultArray: [CNContact] = getAllContacts()
        contactsArray = filteredResultArray.filter({ (contacts) -> Bool in
            return (contacts.givenName.lowercased().contains(text.lowercased()))
        })
    }

    func reloadDataContacts() {
        switch searchController.isActive {
        case false:
            contactsArray = getAllContacts()
        case true:
            if searchController.searchBar.text == ""{
                contactsArray = getAllContacts()
            } else {
                filterContentFor(searchText: searchController.searchBar.text!)
            }
        }
        tableView.reloadData()
    }
    
    func getAllContacts() -> [CNContact] {
        
        let contacts: [CNContact] = {
            let contactStore = CNContactStore()
            let keysToFetch = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                CNContactEmailAddressesKey,
                CNContactPhoneNumbersKey,
                CNContactImageDataAvailableKey,
                CNContactThumbnailImageDataKey] as [Any]
            
            var allContainers: [CNContainer] = []
            do {
                allContainers = try contactStore.containers(matching: nil)
            } catch {
                
            }
            var results: [CNContact] = []
            
            for container in allContainers {
                let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                do {
                    let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                    results.append(contentsOf: containerResults)
                } catch {
                    
                }
            }
            return results
        }()
        return contacts
    }
}

//MARK:Setup
extension ContactsController {
    
    func setupView() {
        
        self.navigationItem.title = "Контакты"
        setupSearch()
        setupTableView()
    }
    
    func setupSearch() {
        // For beuty animation :)
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.barTintColor = AppColor.greenColor()
        searchController.searchBar.tintColor = UIColor.white
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.register(ContactsCell.nib(), forCellReuseIdentifier: ContactsCell.reuseIdentifier())
    }
}

//MARK: UITableViewDataSource
extension ContactsController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactsCell.reuseIdentifier(), for: indexPath) as! ContactsCell
        
        cell.contacts = contactsArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true )
        let value = contactsArray[indexPath.row]
        contactsDelegate?.contactsValue(value: "\(value.givenName) \(value.familyName)")
        searchController.isActive = false
        searchController.searchBar.text = nil
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsArray.count
    }
}

extension ContactsController: UISearchResultsUpdating{
    
    func updateSearchResults(for searchController: UISearchController) {
        reloadDataContacts()
    }

}

extension ContactsController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        searchController.isActive = false
        searchController.searchBar.text = nil
        reloadDataContacts()
        view.endEditing(true)
    }
}

