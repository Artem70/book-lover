//
//  RegisterController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 11.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import PKHUD

class RegisterController: BaseController, ActiveFieldFrameGetter {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var firstnameTextField: TextField!
    @IBOutlet weak var lastnameTextField: TextField!
    @IBOutlet weak var emailTextField: TextField!
    @IBOutlet weak var passwordTextField: TextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    fileprivate var keyboardNotificationHandler: KeyboardNotificationHandler = KeyboardNotificationHandler()
    
    var activeFieldViewFrame: CGRect? {
        guard let activeField = activeField() else {
            return nil
        }
        return activeField.convert(activeField.frame, to: view)
    }
    
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadTextFieldValue()
        keyboardNotificationHandler.subscribeToKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        
        keyboardNotificationHandler.unSubscribeFromKeyboardNotifications()
    }
}

//MARK: Setup
extension RegisterController {
    
    func activeField() -> UIView? {
        return [firstnameTextField,lastnameTextField,emailTextField, passwordTextField].first(where: {$0.isFirstResponder})
    }
    
    func setupView() {
        setupButton()
        setupViewVavBar()
        
//        firstnameTextField.nextField = lastnameTextField
//        lastnameTextField.nextField = emailTextField
//        emailTextField.nextField = passwordTextField
        
        keyboardNotificationHandler.setup(withView: view,
                                          scrollView: scrollView,
                                          activeFrameGetter: self)
    }
    
    func setupViewVavBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(saveButtonPressed(_:)))
        
        self.navigationItem.title = "Регистрация"
    }
    
    func setupButton () {
       button.setImage(#imageLiteral(resourceName: "profileImage"), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 250)
        button.imageView?.contentMode = .scaleAspectFill
    }
    
    func reloadTextFieldValue() {
        firstnameTextField.text = ""
        lastnameTextField.text = ""
        emailTextField.text = ""
        passwordTextField.text = ""
    }

}


//MARK: Action
extension RegisterController {
    
    @IBAction func photoButtonPresed(_ sender: UIButton) {
        
        view.endEditing(true)
        
        let alertcontroller = UIAlertController(title: "Источник фотографии", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (action) in
            self.chooseImagePickerAction(source: .camera)
        }
        let photoLibraryAction = UIAlertAction(title: "Фото(библиотека)", style: .default) { (action) in self.chooseImagePickerAction(source: .photoLibrary)
            
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alertcontroller.addAction(cameraAction)
        alertcontroller.addAction(photoLibraryAction)
        alertcontroller.addAction(cancelAction)
        
        self.present(alertcontroller, animated: true, completion: nil)
    }
    
    func chooseImagePickerAction(source: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            self.present(imagePicker, animated: true, completion: nil )
        }
    }
    
    @objc func saveButtonPressed(_ sender: UIBarButtonItem) {
        
        self.view.endEditing(true)
        
        guard let firstName = firstnameTextField.text, let lastName = lastnameTextField.text, let email = emailTextField.text, let password = passwordTextField.text, firstName != "", lastName != "",email != "", password != "" else {
            showAlertError(mesageTitle: "Заполните все поля", message: "Попробуйте еще раз")
            return
        }
        
        let user = UserData(first: firstName, last: lastName, email:  email, password: password)
        
        let imageData = button.imageView?.image
        
        guard let data = UIImageJPEGRepresentation(imageData!, 1) else {
            print("What is it?")
            return
        }
        
        HUD.show(.progress)
        //send to server POST
        UserRequestManager.instance.createUser(user, imageData: data, success: { [weak self] userResponse in
                    
            AppSettings.sharedInstance.userId = userResponse.id
            AppSettings.sharedInstance.userAvatar = userResponse.avatar_url
            
            AppSettings.sharedInstance.attributeSave(newValue: userResponse.firstname, keyType: .firstname)
            AppSettings.sharedInstance.attributeSave(newValue: userResponse.lastname, keyType: .lastname)
            AppSettings.sharedInstance.attributeSave(newValue: userResponse.email, keyType: .email)
            AppSettings.sharedInstance.attributeSave(newValue: userResponse.password, keyType: .password)

            UserRequestManager.instance.properties = ({ [weak self] result, error, avatar in
                
                guard let strongSelf = self else {
                    return
                }
                
                if result == true {
                    
                    AppSettings.sharedInstance.userAvatar = avatar
                    if let tc = strongSelf.tabBarController as? TabBarController {
                        tc.reloadControllers()
                    } else {
                        strongSelf.present(TabBarController(), animated: true, completion: nil)
                    }
                    HUD.hide()
                }else{
                    strongSelf.showAlertError(mesageTitle: "Error", message: error)
                }
                HUD.hide()
            })
        }) { [weak self] (error) in
             self?.showAlertError(mesageTitle: "Error", message: error)
            HUD.hide()
        }
    }
}

//MARK: Photo Avatar
extension RegisterController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        button.setImage(info [UIImagePickerControllerEditedImage] as? UIImage, for: .normal)
        
        button.contentMode = .scaleAspectFit
        button.clipsToBounds = true
        dismiss(animated: true, completion: nil)
    }
}

// MARK: UITextFieldDelegate
extension RegisterController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _ = textField.resignFirstResponder()
        _ = (textField as? TextField)?.nextField?.becomeFirstResponder()
        return true
    }
}


