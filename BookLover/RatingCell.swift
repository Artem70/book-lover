//
//  RatingCell.swift
//  BookLover
//
//  Created by Artem Pashkevich on 12.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import FloatRatingView

class RatingCell: UITableViewCell {

    @IBOutlet weak var ratingLabel: UILabel!
    
    @IBOutlet var floatRatingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Required float rating view params
        floatRatingView.emptyImage = #imageLiteral(resourceName: "StarEmpty")
        floatRatingView.fullImage = #imageLiteral(resourceName: "StarFull")
        // Optional params
        floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        floatRatingView.maxRating = 5
        floatRatingView.minRating = 0
        //floatRatingView.rating = 2.5
        floatRatingView.editable = true
        floatRatingView.halfRatings = true
        floatRatingView.floatRatings = false

        
        // Initialization code
    }
}
