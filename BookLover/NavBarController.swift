//
//  NavBarController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class NavBarController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func createNavBar(viewController: UIViewController) -> NavBarController {
        return NavBarController(rootViewController: viewController)
    }
}
