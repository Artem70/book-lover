//
//  FileManager+Documents.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit

extension FileManager {
    
    static func documentsDirectory() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    static func pathToImage(avatar_url: String) -> String {
        let documents = documentsDirectory()
        return documents.appendingPathComponent(avatar_url).path
    }
    
    static func saveImageDocumentDirectory(image: UIImage, avatar_url: String) {
        
        let fileManager = FileManager.default
        
        let imageData = UIImageJPEGRepresentation(image, 1)
        
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(avatar_url)
        
        print(paths)
        
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
}
