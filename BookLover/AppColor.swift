//
//  AppColor.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit

class AppColor {
    
    static func greenColor() -> UIColor {
        return UIColor(displayP3Red: 72/255.0, green: 146/255.0, blue: 33/255.0, alpha: 1.0)
    }
}
