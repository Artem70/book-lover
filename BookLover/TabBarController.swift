//
//  TabBarController.swift
//  BookLover
//
//  Created by Artem Pashkevich on 09/09/2017.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadControllers()
    }
}

//MARK: Controllerss generating
extension TabBarController {
    
    func reloadControllers() {
        
        let bookListController = BookListController()
        let profileController = ProfileController()
        let userRegisterController = UserRegisterController()
            
        if let _ = AppSettings.sharedInstance.userId {
            
            viewControllers = [
                NavBarController.createNavBar(viewController: bookListController),
                NavBarController.createNavBar(viewController: profileController),
            ]
        } else {
            
            viewControllers = [
                NavBarController.createNavBar(viewController: bookListController),
                NavBarController.createNavBar(viewController: userRegisterController),
            ]
        }
    }
}
    



