//
//  UIButton+CachedImage.swift
//  BookLover
//
//  Created by Artem Pashkevich on 08.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

extension UIButton {
    
    func setImage(url: String?) {
        
        guard let validUrl = url else {
            return
        }
        
        //HUD.dimsBackground = false
        HUD.show(.progress, onView: self)
        
        let pathToImage = FileManager.pathToImage(avatar_url: validUrl)
        
        if FileManager.default.fileExists(atPath: pathToImage) {
            let image = UIImage(contentsOfFile: pathToImage)
            setImage(image, for: .normal)
            HUD.hide()
        } else {
            UIImageView.imageFromUrl(urlString: url, completionHandler: { (image) in
                self.setImage(image, for: .normal)
                
                FileManager.saveImageDocumentDirectory(image: image!, avatar_url: validUrl)
                HUD.hide()
            })
        }
    }
}
