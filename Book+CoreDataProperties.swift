//
//  Book+CoreDataProperties.swift
//  
//
//  Created by Artem Pashkevich on 13.09.17.
//
//

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book")
    }

    @NSManaged public var author: String?
    @NSManaged public var name: String?
    @NSManaged public var rating: Float
    @NSManaged public var readBook: Bool
    @NSManaged public var whoAdvised: String?

}
